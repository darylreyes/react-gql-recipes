const { gql } = require('apollo-server');

module.exports = gql`
  type File {
    _id: ID!
    filename: String!
    mimetype: String!
    path: String!
  }

  type Post {
    id: ID!
    body: String!
    user: User
    imageId: ID!
    createdAt: String!
  }
  type Comment {
    id: ID!
    createdAt: String!
    username: String!
    body: String
  }
  type Like {
    id: ID!
    createdAt: String!
    username: String!
  }
  type User {
    id: ID!
    firstName: String!
    lastName: String!
    email: String!
    username: String!
    password: String!
    confirmPassword: String!
    token: String!
    displayPhoto: File
    createdAt: String!
    UpdatedAt: String!
  }

  input RegisterInput {
    firstName: String!
    lastName: String!
    email: String!
    username: String!
    password: String!
    displayPhoto: Upload
    confirmPassword: String!
  }

  type Query {
    getPost: [Post]
    user: User
  }
  type Mutation {
    register(RegisterInput: RegisterInput): User!
    login(email: String!, password: String!): User!
    createPost(body: String!, Image: ID): Post!
    uploadFile(file: Upload!): File
  }
`;
