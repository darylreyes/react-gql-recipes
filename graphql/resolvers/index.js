const postResolvers = require('./project-resolvers/post');
const userResolver = require('./project-resolvers/user');
const fileResolver = require('./project-resolvers/files');

module.exports = {
  Query: {
    ...postResolvers.Query
  },
  Mutation: {
    ...userResolver.Mutation,
    ...postResolvers.Mutation,
    ...fileResolver.Mutation
  }
};
