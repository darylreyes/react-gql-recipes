const checkAuth = require('../../../utils/checkAuth');
const Post = require('../../../models/Post.model');

module.exports = {
  Query: {
    getPost: async () => {
      const posts = await Post.find({}).populate('user');
      return posts;
    }
  },
  Mutation: {
    createPost: async (_, { body, image }, context) => {
      const user = checkAuth(context);
      console.log(user);
      if (!body) throw new Error('Post body must not be empty');
      const newPost = new Post({
        body,
        image,
        user: user.id,
        createdAt: new Date().toISOString()
      });
      const post = await newPost.save();
      return post;
    }
  }
};
