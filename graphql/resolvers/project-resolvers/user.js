const User = require('../../../models/User.model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { UserInputError } = require('apollo-server');
const {
  validateRegisterInput,
  validateLoginInput
} = require('../../../utils/validator');

const generateToken = user => {
  return jwt.sign(
    {
      id: user.id,
      email: user.email,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName
    },
    process.env.SECRET_KEY,
    { expiresIn: '1h' }
  );
};

module.exports = {
  Mutation: {
    register: async (
      _,
      {
        RegisterInput: {
          firstName,
          lastName,
          username,
          email,
          password,
          confirmPassword
        }
      }
    ) => {
      const { errors, valid } = validateRegisterInput(
        firstName,
        lastName,
        username,
        email,
        password,
        confirmPassword
      );
      if (!valid) {
        throw new UserInputError('Errors', { errors });
      }
      const user = await User.findOne({
        $or: [{ username }, { email }]
      });
      if (user) {
        if (user.username === username) {
          throw new UserInputError('Username is taken', {
            errors: {
              username: 'This username is taken'
            }
          });
        }
        if (user.email === email) {
          throw new UserInputError('This email is already exis', {
            errors: {
              email: 'This email is already exist'
            }
          });
        }
      }
      password = await bcrypt.hash(password, 12);
      const newUser = new User({
        firstName,
        lastName,
        email,
        username,
        password,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString()
      });
      const res = await newUser.save();
      const token = generateToken(res);
      return {
        ...res._doc,
        id: res.id,
        token
      };
    },
    login: async (_, { email, password }) => {
      const { errors, valid } = validateLoginInput(email, password);
      if (!valid) {
        throw new UserInputError('Errors', { errors });
      }
      const user = await User.findOne({ email });
      console.log(user);
      if (!user) {
        errors.general = 'User not found';
        throw new UserInputError('User not found', { errors });
      }
      const match = await bcrypt.compare(password, user.password);
      if (!match) {
        errors.general = 'Wrong Credentials';
        throw new UserInputError('Wrong Credentials', { errors });
      }
      const token = generateToken(user);
      return {
        ...user._doc,
        id: user.id,
        token
      };
    }
  }
};
