const Files = require('../../../models/FileUpload.model');
const { createWriteStream } = require('fs');
const storeUpload = async ({ stream, filename, mimetype }) => {
  // const id = shortid.generate();
  const path = `fileUploads/${filename}`;
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on('finish', () => resolve({ path, filename, mimetype }))
      .on('error', reject)
  );
};
const processUpload = async upload => {
  const { createReadStream, filename, mimetype } = await upload;
  const stream = createReadStream();
  const file = await storeUpload({ stream, filename, mimetype });
  return file;
};
module.exports = {
  Query: {
    uploads: (parent, args) => {}
  },
  Mutation: {
    uploadFile: async (_, { file }) => {
      console.log(file);
      const upload = await processUpload(file);
      // save our file to the mongodb
      const files = await Files.create(upload);
      console.log(files);
      return files;
    }
  }
};
