const { model, Schema } = require('mongoose');

const userSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  username: String,
  password: String,
  createdAt: String,
  updatedAt: String,
  displayPhoto: {
    type: Schema.Types.ObjectId,
    ref: 'FileUploads'
  }
});

module.exports = model('User', userSchema);
