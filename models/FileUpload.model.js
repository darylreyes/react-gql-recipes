const { Schema, model } = require('mongoose');
const fileUploadSchema = new Schema({
  filename: String,
  mimetype: String,
  path: String
});
module.exports = model('FileUploads', fileUploadSchema);
