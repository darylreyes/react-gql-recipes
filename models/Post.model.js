const { model, Schema } = require('mongoose');

const postSchema = new Schema({
  body: String,
  createdAt: String,
  image: {
    type: Schema.Types.ObjectId,
    ref: 'FileUploads'
  },
  comments: [
    {
      body: String,
      user: String,
      createdAt: String
    }
  ],
  likes: [
    {
      user: String,
      createdAt: String
    }
  ],
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

module.exports = model('Post', postSchema);
