const { ApolloServer, PubSub, gql } = require('apollo-server-express');
const mongoose = require('mongoose');
const resolvers = require('./graphql/resolvers');
const typeDefs = require('./graphql/typeDefs');
const express = require('express');
const cors = require('cors');
require('dotenv').config();

const app = express();

const pubsub = new PubSub('ws://localhost:5000/', {
  reconnect: true
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ req, pubsub })
});

// const serverStart = async () => {
//   try {
//     const res = await server.listen({ port: 5000 });
//     console.log(`Server running at ${res.url}`);
//   } catch (error) {
//     console.log(error);
//   }
// };

// serverStart();

app.use('/fileUploads', express.static('fileUploads'));
app.use(cors('*'));

server.applyMiddleware({ app });

app.listen(5000, () => {
  console.log('Server running @ http://localhost:5000/graphql');
});

const connection = async () => {
  try {
    const con = await mongoose.connect(process.env.MONGODB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true
    });
    if (con) console.log('MongoDB Connected');
  } catch (error) {
    console.log(error);
  }
};
connection();
