const jwt = require('jsonwebtoken');
// process.env.SECRET_KEY
const { AuthenticationError } = require('apollo-server');

module.exports = context => {
  const authHeader = context.req.headers.authorization;

  if (authHeader) {
    const token = authHeader.split('Bearer ')[1];
    if (token) {
      try {
        const user = jwt.verify(token, process.env.SECRET_KEY);
        return user;
      } catch (error) {
        throw new AuthenticationError('Invalid/Expired Token');
      }
    }
    throw new Error('Authentication token must be Bearer [token] ');
  }
  throw new Error('Authoriztaion herder must be provided');
};
