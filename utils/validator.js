module.exports.validateRegisterInput = (
  firstName,
  lastName,
  username,
  email,
  password,
  confirmPassword
) => {
  const errors = {};
  if (!firstName) {
    errors.firstName = 'First Name must not empty';
  }
  if (!lastName) {
    errors.lastName = 'Last Name must not empty';
  }
  if (!username) {
    errors.username = 'Username must not empty';
  }
  if (!email) {
    errors.email = 'Email must not empty';
  } else {
    const regEx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if (!email.match(regEx)) {
      errors.email = 'Email must be a valid email address';
    }
  }
  if (!password) {
    errors.password = 'Password must not empty';
  } else if (password.length < 6) {
    errors.password = 'Password needs to have atleast 6 characters';
  } else if (password !== confirmPassword) {
    errors.confirmPassword = 'Password must match to confirm password';
  }

  return {
    errors,
    valid: !Object.keys(errors).length
  };
};

module.exports.validateLoginInput = (email, password) => {
  const errors = {};
  if (!email) {
    errors.email = 'Email must not empty';
  } else {
    const regEx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
    if (!email.match(regEx)) {
      errors.email = 'Email must be a valid email address';
    }
  }
  if (!password) {
    errors.password = 'Password must not empty';
  }
  return {
    errors,
    valid: !Object.keys(errors).length
  };
};
