import React from 'react';
import PropTypes from 'prop-types';
import MenuBar from './MenuBar';
import FooterBar from './FooterBar';

function NavigationWrapper({ children }) {
  return (
    <div className="page-container">
      <MenuBar />
      <div className="content-wrap">{children}</div>
      <FooterBar />
    </div>
  );
}

NavigationWrapper.propTypes = {};

export default NavigationWrapper;
