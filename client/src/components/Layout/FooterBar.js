import React from 'react';
import PropTypes from 'prop-types';
import { colors, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  mainFooter: {
    backgroundColor: ' #e60000',
    color: 'white',
    textAlign: 'center',
    // paddingTop: '2em',
    position: 'relative',
    bottom: 0,
    width: '100%'
  }
}));

function FooterBar(props) {
  const classes = useStyles();
  return (
    <div className={classes.mainFooter}>
      <p>
        Terms and Conditions | Privact Policy | All rights reserved &copy;
        {new Date().getFullYear()}
      </p>
    </div>
  );
}

FooterBar.propTypes = {};

export default FooterBar;
