import { library } from '@fortawesome/fontawesome-svg-core';
import { faPepperHot } from '@fortawesome/free-solid-svg-icons';

library.add(faPepperHot);
