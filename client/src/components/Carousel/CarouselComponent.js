import React from 'react';
import PropTypes from 'prop-types';
import { Carousel } from 'react-responsive-carousel';

function CarouselComponent({ imageUrls = [], style = {} }) {
  return (
    <Carousel
      showThumbs={false}
      autoPlay
      infiniteLoop
      showStatus={false}
      showArrows
    >
      {imageUrls.map((imageUrl, index) => {
        return (
          <div
            key={index}
            style={{
              backgroundImage: `url(${imageUrl})`,
              ...style
            }}
          />
        );
      })}
    </Carousel>
  );
}

CarouselComponent.propTypes = {
  data: PropTypes.array,
  style: PropTypes.object
};

export default CarouselComponent;
