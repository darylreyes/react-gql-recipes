import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useMutation } from '@apollo/react-hooks';
import FileUploadMutation from '../../gql/FileUpload.Mutation';
function FileUploadForm({ getFileId }) {
  const [file, setFile] = useState();
  const [uploadFile] = useMutation(FileUploadMutation);

  const handleChange = async e => {
    // setFile(e.target.files[0]);

    try {
      const datas = e.target.files[0];
      const data = await uploadFile({
        variables: {
          file: datas
        }
      });
      console.log(data);
      //   getFileId(data);
      setFile(data);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div>
      <img
        src={file ? `http://localhost:5000/${file.data.uploadFile.path}` : ''}
      />
      <input type="file" onChange={handleChange} />
    </div>
  );
}

FileUploadForm.propTypes = {
  getFileId: PropTypes.func
};

export default FileUploadForm;
