import React from 'react';
import PropTypes from 'prop-types';
import { Button, TextField } from '@material-ui/core';
import FileUploadForm from './FileUploadForm';

function PostForm(props) {
  const getFileId = fileId => {
    console.log(fileId);
  };
  return (
    <div style={{ padding: '10px' }}>
      <h2>Create a post: </h2>
      <TextField
        id="outlined-full-width"
        style={{ margin: 8 }}
        placeholder="Hi..."
        fullWidth
        margin="normal"
        InputLabelProps={{
          shrink: true
        }}
        variant="outlined"
      />
      <FileUploadForm getFileId={getFileId} />
      <Button>Submit</Button>
    </div>
  );
}

PostForm.propTypes = {};

export default PostForm;
