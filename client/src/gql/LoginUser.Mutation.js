const gql = require('graphql-tag');

export default gql`
  mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      email
      token
      username
      createdAt
    }
  }
`;
