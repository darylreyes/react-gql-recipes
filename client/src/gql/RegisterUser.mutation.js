const gql = require('graphql-tag');

export default gql`
  mutation(
    $username: String!
    $password: String!
    $firstName: String!
    $lastName: String!
    $email: String!
    $confirmPassword: String!
  ) {
    register(
      RegisterInput: {
        username: $username
        password: $password
        email: $email
        confirmPassword: $confirmPassword
        lastName: $lastName
        firstName: $firstName
      }
    ) {
      id
      email
      token
      username
      createdAt
    }
  }
`;
