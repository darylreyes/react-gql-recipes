const gql = require('graphql-tag');

export default gql`
  mutation($file: Upload!) {
    uploadFile(file: $file) {
      _id
      filename
      mimetype
      path
    }
  }
`;
