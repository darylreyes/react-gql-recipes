import React, { useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Grid, Paper, TextField } from '@material-ui/core';

import CarouselComponent from '../components/Carousel/CarouselComponent';
import Login from './Login';

import Register from './Register';

function AuthComponent({ history, match }) {
  const [isLogin, setIsLogin] = useState(true);

  useEffect(() => {
    if (match.params.type === 'login') {
      setIsLogin(true);
    } else {
      setIsLogin(false);
    }
  }, [match.params.type]);

  const imageUrls = [
    '/images/food1.jpeg',
    '/images/food3.jpeg',
    '/images/food4.jpeg',
    '/images/food5.jpeg'
  ];

  return (
    <div className="loginMain">
      <div className="loginContainer">
        <Grid container className="gridContainer">
          <Grid className="carouselGrid" item xs={12} sm={12} md={12} lg={8}>
            <CarouselComponent
              imageUrls={imageUrls}
              style={{
                height: '100%',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: '50% 50%'
              }}
            />
          </Grid>
          {isLogin ? (
            <Login
              history={history}
              //   handleSwitchRegister={() =>  }
            />
          ) : (
            <Register
              history={history}
              handleSwitchRegister={() => setIsLogin(true)}
            />
          )}
        </Grid>
      </div>
    </div>
  );
}

AuthComponent.propTypes = {};

export default AuthComponent;
