import React from 'react';
import PropTypes from 'prop-types';
import { compareSync } from 'bcryptjs';
import { Grid } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import PostForm from '../components/Post/PostForm';

function Home(props) {
  const handleClick = e => {
    console.log(e.target);
  };
  return (
    <Grid container>
      <Grid item xs={3}>
        <PostForm />
      </Grid>
      <Grid item xs={9}>
        <Pagination onClick={handleClick} count={10} />
      </Grid>
    </Grid>
  );
}

Home.propTypes = {};

export default Home;
