import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Grid, TextField } from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useMutation } from '@apollo/react-hooks';
import LoginUserMutation from '../gql/LoginUser.Mutation';
import { AuthContext } from '../contexts/auth';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
function Login({ history, handleSwitchRegister }) {
  const context = useContext(AuthContext);
  const [values, setValues] = useState({
    email: '',
    password: ''
  });
  const [errors, setErrors] = useState({});

  const [loginUser] = useMutation(LoginUserMutation);
  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' });
  };
  const handleSubmit = async e => {
    e.preventDefault();
    try {
      const { data } = await loginUser({ variables: values });
      if (data) {
        context.login(data.login);
        setValues({
          email: '',
          password: ''
        });
        history.push('/');
      }
    } catch (error) {
      console.log(error);
      toast.error(error.graphQLErrors[0].extensions.exception.errors.general);
      console.log(error.graphQLErrors[0].extensions.exception.errors);
      setErrors(error.graphQLErrors[0].extensions.exception.errors);
    }
  };
  return (
    <Grid
      item
      xs={12}
      sm={12}
      md={12}
      lg={4}
      style={{ padding: '0px 30px', margin: 'auto auto' }}
    >
      <h2 style={{ color: '#ECBB5C ', textAlign: 'center' }}>Login</h2>

      <TextField
        required
        name="email"
        label="Email"
        placeholder="Email"
        fullWidth
        style={{ width: '100%' }}
        value={values.email}
        onChange={handleChange}
        error={!!errors.email}
        helperText={errors.email}
        InputProps={{
          endAdornment: <AccountCircle style={{ color: '#ECBB5C ' }} />
        }}
      />
      <TextField
        required
        name="password"
        label="Password"
        placeholder="Password"
        fullWidth
        value={values.password}
        onChange={handleChange}
        error={!!errors.password}
        helperText={errors.password}
        style={{ marginTop: '10px' }}
      />
      <div style={{ textAlign: 'center' }}>
        <Button
          variant="outlined"
          color="primary"
          onClick={handleSubmit}
          style={{
            marginTop: '20px',
            color: 'black',
            backgroundColor: '#ECBB5C '
          }}
        >
          Login
        </Button>
      </div>
      <Button
        component={Link}
        to="/account/register"
        style={{ color: '#ECBB5C ' }}
      >
        Create Account
      </Button>
    </Grid>
  );
}

Login.propTypes = {};

export default Login;
