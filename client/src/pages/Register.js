import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import { useMutation } from '@apollo/react-hooks';
import RegisterUserMutation from '../gql/RegisterUser.mutation';
import { AuthContext } from '../contexts/auth';
import { Link } from 'react-router-dom';
function Register({ history, handleSwitchRegister }) {
  const context = useContext(AuthContext);
  const [values, setValues] = useState({
    firstName: '',
    lastName: '',
    email: '',
    username: '',
    password: '',
    confirmPassword: ''
  });
  const [errors, setErrors] = useState({});

  const [addUser] = useMutation(RegisterUserMutation);
  const handleChange = e => {
    setValues({ ...values, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' });
  };
  const handlSubmit = async e => {
    e.preventDefault();
    try {
      const { data } = await addUser({ variables: values });
      if (data) {
        context.login(data.register);
        setValues({
          firstName: '',
          lastName: '',
          email: '',
          username: '',
          password: '',
          confirmPassword: ''
        });
        history.push('/');
      }
    } catch (error) {
      console.log(error);
      console.log(error.graphQLErrors[0].extensions.exception.errors);
      setErrors(error.graphQLErrors[0].extensions.exception.errors);
    }
  };
  // console.log(errors);
  return (
    <Grid
      item
      xs={12}
      sm={12}
      md={12}
      lg={4}
      container
      style={{ margin: 'auto auto', padding: '0px 20px' }}
      spacing={1}
    >
      <Grid item xs={12} style={{ padding: 0, margin: 0 }}>
        <h2 style={{ color: '#ECBB5C ', textAlign: 'center', margin: 0 }}>
          Register
        </h2>
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          name="firstName"
          label="First Name"
          placeholder="First Name"
          fullWidth
          size="small"
          value={values.firstName}
          onChange={handleChange}
          error={!!errors.firstName}
          helperText={errors.firstName}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          label="Last Name"
          name="lastName"
          placeholder="Last Name"
          fullWidth
          size="small"
          value={values.lastName}
          onChange={handleChange}
          error={!!errors.lastName}
          helperText={errors.lastName}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          label="Email"
          name="email"
          placeholder="Email"
          fullWidth
          size="small"
          value={values.email}
          onChange={handleChange}
          error={!!errors.email}
          helperText={errors.email}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          label="Username"
          name="username"
          placeholder="Username"
          fullWidth
          size="small"
          value={values.username}
          onChange={handleChange}
          error={!!errors.username}
          helperText={errors.username}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          type="password"
          label="Password"
          name="password"
          placeholder="Password"
          fullWidth
          size="small"
          value={values.password}
          onChange={handleChange}
          error={!!errors.password}
          helperText={errors.password}
        />
      </Grid>
      <Grid item xs={6}>
        <TextField
          required
          type="password"
          label="Confirm Password"
          name="confirmPassword"
          placeholder="Confirm Password"
          fullWidth
          size="small"
          value={values.confirmPassword}
          onChange={handleChange}
          error={!!errors.confirmPassword}
          helperText={errors.confirmPassword}
        />
      </Grid>
      <Grid
        item
        xs={12}
        style={{
          textAlign: 'center'
        }}
      >
        <Button
          variant="outlined"
          color="primary"
          onClick={handlSubmit}
          style={{
            color: 'black',
            backgroundColor: '#ECBB5C '
          }}
        >
          Register
        </Button>
      </Grid>
      <Grid item xs={12}>
        Have already an account ?
        <Button
          component={Link}
          to="/account/login"
          style={{ color: '#ECBB5C ' }}
        >
          Login Here
        </Button>
      </Grid>
    </Grid>
  );
}

Register.propTypes = {};

export default Register;
