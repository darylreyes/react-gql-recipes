import React from 'react';
import './App.css';
import './components/FontAwesomeIcons';
import { AuthProvider } from './contexts/auth';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import AuthRoute from './utils/AuthRoute';
// import Login from './pages/Login';
import Account from './pages/AuthComponent';
import { CssBaseline } from '@material-ui/core';
import NavigationWrapper from './components/Layout/NavigationWrapper';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <AuthProvider>
      <Router>
        <NavigationWrapper>
          <ToastContainer position="bottom-right" autoClose={5000} />
          <CssBaseline />
          <Route exact path="/" component={Home} />
          <AuthRoute exact path="/account/:type" component={Account} />
        </NavigationWrapper>
      </Router>
    </AuthProvider>
  );
}

export default App;
